use tfhe::prelude::*;
use tfhe::{generate_keys, set_server_key, ConfigBuilder, FheBool, FheUint8};

mod circuits;
use crate::circuits::booleanCircuits::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    use std::time::Instant;
    let mut now = Instant::now();

    let config = ConfigBuilder::default().build();
    let (keys, server_keys) = generate_keys(config);
    set_server_key(server_keys);

    let mut elapsed = now.elapsed();
    println!("TIMING--config/key gen: {:.2?}\n", elapsed);
    
    let mut fhe_true = FheBool::try_encrypt(true, &keys)?;
    let mut fhe_false = FheBool::try_encrypt(false, &keys)?;

    // bitwise blind comparison
    let mut now = Instant::now();

    let mut result1 = fhe_true.clone() & !fhe_false.clone();
    let mut result2 = !fhe_true.clone() & fhe_false.clone();

    let mut elapsed = now.elapsed();
    println!("TIMING--bitwise blind comp: {:.2?}\n", elapsed);

    let dec_gt:bool = result1.decrypt(&keys);
    let dec_lt:bool = result2.decrypt(&keys);

    println!("{:?}, {:?}", dec_gt, dec_lt);

    // blind comparison
    println!("multi-bit blind comparison");
    let mut vec1 = [fhe_false.clone(), fhe_true.clone(), fhe_true.clone(), fhe_false.clone()];
    let mut vec2 = [fhe_true.clone(), fhe_false.clone(), fhe_true.clone(), fhe_true.clone()];

    let mut now = Instant::now();
    let mut res2 = twoBitComparator(&vec1, &vec2);
    let mut elapsed = now.elapsed();
    println!("TIMING--blind comp 2-bit: {:.2?}\n", elapsed);
    let dec_gt:bool = res2.0.decrypt(&keys);
    let dec_lt:bool = res2.1.decrypt(&keys);
    println!("2: {:?}, {:?}", dec_gt, dec_lt);

    let mut now = Instant::now();
    let mut res3 = threeBitComparator(&vec1, &vec2);
    let mut elapsed = now.elapsed();
    println!("TIMING--blind comp 3-bit: {:.2?}\n", elapsed);
    let dec_gt:bool = res3.0.decrypt(&keys);
    let dec_lt:bool = res3.1.decrypt(&keys);
    println!("3: {:?}, {:?}", dec_gt, dec_lt);

    let mut now = Instant::now();
    let mut res4 = fourBitComparator(&vec1, &vec2);
    let mut elapsed = now.elapsed();
    println!("TIMING--blind comp 4-bit: {:.2?}\n", elapsed);
    let dec_gt:bool = res4.0.decrypt(&keys);
    let dec_lt:bool = res4.1.decrypt(&keys);
    println!("4: {:?}, {:?}", dec_gt, dec_lt);

    let mut vec = vec![
        vec![fhe_true.clone(), fhe_false.clone(), fhe_true.clone(), fhe_true.clone()],
        vec![fhe_false.clone(), fhe_true.clone(), fhe_true.clone(), fhe_true.clone()],
        vec![fhe_true.clone(), fhe_true.clone(), fhe_true.clone(), fhe_true.clone()]
    ];
    println!("original list: [1011, 0111, 1111]");

    // blind min/max
    println!("4-bit argmin");
    let mut now = Instant::now();

    let mut min = findMin(&vec);
    
    let mut elapsed = now.elapsed();
    println!("TIMING--min/max: {:.2?}\n", elapsed);
    
    let mut dec_min = vec![];
    for bit in min {
        if bit.decrypt(&keys) {
            dec_min.push(1);
        } else {
            dec_min.push(0);
        }
    }
    println!("arg min: {:?}", dec_min);

    println!("4-bit argmax");
    let mut now = Instant::now();

    let mut max = findMax(&vec);
    
    let mut elapsed = now.elapsed();
    println!("TIMING--min/max: {:.2?}\n", elapsed);
    
    let mut dec_max = vec![];
    for bit in max {
        if bit.decrypt(&keys) {
            dec_max.push(1);
        } else {
            dec_max.push(0);
        }
    }
    println!("arg max: {:?}", dec_max);

    // blind sorting
    println!("blind sorting");
    now = Instant::now();

    let n = vec.len();
    for i in 1..n {
        let mut j = i;
        while j > 0 {
            let (mut ifResult, mut elseResult) = funcGT(&vec[j-1], &vec[j]);
            let mut res1:Vec<FheBool> = Vec::new();
            let mut res2:Vec<FheBool> = Vec::new();

            (res1, res2) = hotSwap(&ifResult, &elseResult, &vec[j-1], &vec[j]);
            vec[j-1] = res1;
            vec[j] = res2;

            j = j - 1
        }
    }

    elapsed = now.elapsed();
    println!("TIMING--sorting: {:.2?}", elapsed);

    println!("printing the sorted array");

    for arr in &vec {
        let mut dec_vec = vec![];
        for bit in arr {
            if bit.decrypt(&keys) {
                dec_vec.push(1);
            } else {
                dec_vec.push(0);
            }
        }
        let result:u8 = convert(&dec_vec);
        print!("{:?} ", result);
    }   println!("");

    Ok(())
}
