/*
    PLAINTEXT VERSION OF THE DECISION_TREE PROJECT FOR TESTING ALGORITHMS BEFORE IMPLEMENTING THEM IN FHE
*/

use std::collections::VecDeque;
use std::collections::HashMap;

// static mut SOLUTIONS:Vec<Vec<FheBool>> = Vec::new();

struct Dataset {
    features: Vec<Vec<usize>>,
    labels: Vec<bool>
}

#[derive(Debug)]
struct BinaryTree {
    feature: Option<usize>,
    threshold: Option<usize>,
    value: Option<usize>,
    left: Option<Box<BinaryTree>>,
    right: Option<Box<BinaryTree>>
}
impl BinaryTree {
    fn new(feature: Option<usize>, threshold: Option<usize>, value: Option<usize>) -> Self {
        BinaryTree {
            feature: feature,
            threshold: threshold,
            value: value,
            left: None,
            right: None
        }
    }

    fn in_order_traversal(&self) {
        if let Some(ref left) = self.left {
            left.in_order_traversal();
        }
        // println!("{:?}", self.value);
        if let Some(ref right) = self.right {
            right.in_order_traversal();
        }
    }
    fn post_order_traversal(&self) {
        if let Some(ref left) = self.left {
            left.post_order_traversal();
        }
        if let Some(ref right) = self.right {
            right.post_order_traversal();
        }
        // println!("{:?}", self.value);
    }
    fn pre_order_traversal(&self) {
        // println!("{:?}", self.value);
        if let Some(ref left) = self.left {
            left.pre_order_traversal();
        }
        if let Some(ref right) = self.right {
            right.pre_order_traversal();
        }
    }

    fn insert(&mut self, feature: Option<usize>, threshold: Option<usize>, value: Option<usize>) {
        if value.as_ref().unwrap() < self.value.as_ref().unwrap() {
            if let Some(ref mut left) = self.left {
                left.insert(feature, threshold, value);
            } else {
                self.left = Some(Box::new(BinaryTree::new(feature, threshold, value)));
            }
        } else {
            if let Some(ref mut right) = self.right {
                right.insert(feature, threshold, value);
            } else {
                self.right = Some(Box::new(BinaryTree::new(feature, threshold, value)));
            }
        }
    }

    // breadth-first insertion: 
    fn insert_bf(&mut self, feature:Option<usize>, threshold:Option<usize>, value: Option<usize>) {
        let mut queue = VecDeque::new();
        queue.push_back(self);

        while let Some(node) = queue.pop_front() {
            if node.left.is_none() {
                node.left = Some(Box::new(BinaryTree::new(feature, threshold, value)));
                return;
            } else {
                queue.push_back(node.left.as_mut().unwrap());
            }

            if node.right.is_none() {
                node.right = Some(Box::new(BinaryTree::new(feature, threshold, value)));
                return;
            } else {
                queue.push_back(node.right.as_mut().unwrap());
            }
        }
    }

    // fn decision_tree_traversal(&self, features: &[Option<usize>], flag: &FheBool) {
    //     // println!("position: {:?}", self.position);
    //     if let Some(ref value) = self.value {
    //         println!("one solution found");
    //         let mut temp:usize = Vec::new();
    //         for v in value {
    //             temp.push(v & flag);
    //         }

    //         unsafe {
    //             SOLUTIONS.push(temp);
    //         }
    //     } else {
    //         // println!("feature number: {:?}", self.feature);
    //         // if features[*self.feature.as_ref().unwrap()].as_ref().unwrap() <= self.threshold.as_ref().unwrap() {
    //         //     // println!("gets here");
    //         //     if let Some(ref left) = self.left {
    //         //         left.decision_tree_traversal(features);
    //         //     }
    //         // } else {
    //         //     if let Some(ref right) = self.right {
    //         //         // println!("pos: {:?} gets here", self.position);
    //         //         right.decision_tree_traversal(features);
    //         //     }
    //         // }

    //         let (mut ifResult, mut elseResult) = funcGT(features[*self.feature.as_ref().unwrap()].as_ref().unwrap(), self.threshold.as_ref().unwrap());
            
    //         if let Some(ref left) = self.left {

    //             let mut new_flag = flag & elseResult;
    //             left.decision_tree_traversal(features, &new_flag);
    //         }
    //         if let Some(ref right) = self.right {
    //             let mut new_flag = flag & ifResult;
    //             right.decision_tree_traversal(features, &new_flag);
    //         }
    //     }
    // }
}

fn gini_index(features: &Vec<Vec<usize>>, labels: &Vec<bool>, feature_idx: usize, threshold: usize) -> f32 {
    let mut label_idx = 0;
    let (mut left_count, mut left_label_prop) = (0.0,0.0);
    let (mut right_count, mut right_label_prop) = (0.0,0.0);

    for entry in features {
        if entry[feature_idx] <= threshold {
            left_count = left_count + 1.0;
            if labels[label_idx] {
                left_label_prop = left_label_prop + 1.0;
            }
        } else {
            right_count = right_count + 1.0;
            if labels[label_idx] {
                right_label_prop = right_label_prop + 1.0;
            }
        }
        label_idx += 1;
    }

    let mut left_impurity = 0.0;
    let mut right_impurity = 0.0;

    if left_count > 0.0 {
        left_impurity = (1.0 - left_label_prop/left_count) * (1.0-(1.0-left_label_prop/left_count)) + (left_label_prop/left_count) * (1.0-left_label_prop/left_count);
    }

    if right_count > 0.0 {
        right_impurity = (1.0 - right_label_prop/right_count) * (1.0-(1.0-right_label_prop/right_count)) + (right_label_prop/right_count) * (1.0-right_label_prop/right_count);
    }

    println!("feature impurity");
    let mut feature_impurity = (left_count/(features.len() as f32)) * left_impurity + (right_count/(features.len() as f32)) * right_impurity;
    println!("{:?}", feature_impurity);

    feature_impurity
}

fn choose_feature(feature_impurities: &Vec<(f32, usize)>) -> (f32, usize) {
    let mut index_low = 0;
    let mut gini_low = 99999.99;
    for gini_index in feature_impurities {
        if gini_index.0 < gini_low {
            index_low = gini_index.1;
            gini_low = gini_index.0;
        }
    }

    (gini_low, index_low)
}

fn split_features(features: &Vec<Vec<usize>>, labels: &Vec<bool>, index_low: usize, threshold: usize) -> (Vec<Vec<usize>>, Vec<bool>, Vec<Vec<usize>>, Vec<bool>) {
    let mut left_features = Vec::new();
    let mut left_labels = Vec::new();
    let mut right_features = Vec::new();
    let mut right_labels = Vec::new();

    for (index, row) in features.iter().enumerate() {
        if row[index_low] <= threshold as usize {
            let mut clipped_row = row.clone();
            // clipped_row.remove(index_low);

            left_features.push(clipped_row);
            left_labels.push(labels[index].clone());
        } else {
            let mut clipped_row = row.clone();
            // clipped_row.remove(index_low);

            right_features.push(clipped_row);
            right_labels.push(labels[index].clone());
        }
    }

    (left_features, left_labels, right_features, right_labels)
}

fn main() {
    /*
        f1,f2,f3,L
        2, 4, 1, 1
        1, 3, 2, 0
        3, 1, 1, 1
        4, 2, 2, 1
        2, 1, 2, 0
        4, 4, 1, 1
        3, 3, 2, 1
        1, 4, 1, 0

                    Feature 1 <= 2
                        |
                ----------------------
                |                      |
        Feature 2 <= 3.5        Feature 3 <= 1.5
                |                      |
        ---------------           ---------------
        |               |         |               |
        Label 1        Label 1    Label 1         Label 2

        new feature: [4, 2, 3]
    */

    let mut vec1 = vec![2,4,1];
    let mut vec2 = vec![1,3,2];
    let mut vec3 = vec![3,1,1];
    let mut vec4 = vec![4,2,2];
    let mut vec5 = vec![2,1,2];
    let mut vec6 = vec![4,4,1];
    let mut vec7 = vec![3,3,2];
    let mut vec8 = vec![1,4,1];
    let mut features:Vec<Vec<usize>> = Vec::new();
    features.push(vec1);
    features.push(vec2);
    features.push(vec3);
    features.push(vec4);
    features.push(vec5);
    features.push(vec6);
    features.push(vec7);
    features.push(vec8);
    let mut labels:Vec<bool> = Vec::new();
    labels.push(true);
    labels.push(false);
    labels.push(true);
    labels.push(true);
    labels.push(false);
    labels.push(true);
    labels.push(true);
    labels.push(false);

    // 1. CALCULATE GINI IMPURITY OF ALL FEATURES

    // creating a list of indexes to use to do gini impurities: once a feature is selected and made into a node, that feature's index will be removed from the list
    let mut feature_indices = vec![0,1,2];

    // gini impurity testing on feature 0
    println!("\ncalculating gini impurity of feature 0");
    println!("{:?}", gini_index(&features, &labels, 0, 2));

    // calculating gini impurities of all features
    println!("\ncalculating gini impurities of all features");

    let num_features = features[0].len();
    let threshold = 2.5;
    let mut feature_impurities:Vec<(f32, usize)> = Vec::new();

    for i in 0..num_features {
        let feature_idx = i;
        let feature_impurity = gini_index(&features, &labels, feature_idx, 2);

        feature_impurities.push((feature_impurity, i));
    }

    // find the feature with the lowest gini impurity
    println!("\nchosen feature");
    let (chosen_gini, chosen_index) = choose_feature(&feature_impurities);
    println!("feature: {:?}\tgini index: {:?}", chosen_index, chosen_gini);

    // create the decision tree
    let mut decision_tree:BinaryTree = BinaryTree::new(Some(chosen_index), Some(threshold as usize), None);
    feature_indices.retain(|&value| value != chosen_index);

    // 2. CALCULATE OPTIMAL THRESHOLD VALUE -- skipped for now

    // 3. CALCULATE GINI INDEX FOR LEFT AND RIGHT SUBSETS

    // split the dataset into left and right subsets based on the root node threshold
    println!("\nsplitting the dataset");
    let (left_features, left_labels, right_features, right_labels) = split_features(&features, &labels, chosen_index, 2);

    println!("left subset");
    for (row, label) in left_features.iter().zip(left_labels.iter()) {
        println!("{:?}\t{:?}", row, label);
    }
    println!("right subset");
    for (row, label) in right_features.iter().zip(right_labels.iter()) {
        println!("{:?}\t{:?}", row, label);
    }

    // calculating the gini index of the features in the subsets
    println!("\ncalculating the gini index of subsets");
    let mut feature_impurities:Vec<(f32, usize)> = Vec::new();
    for i in feature_indices.clone() {
        let feature_idx = i;
        let feature_impurity = gini_index(&left_features, &labels, feature_idx, 2);

        feature_impurities.push((feature_impurity, feature_idx));
    }

    println!("chosen feature for left subset");
    let (chosen_gini, chosen_index) = choose_feature(&feature_impurities);
    println!("feature: {:?}\tgini index: {:?}", chosen_index, chosen_gini);

    decision_tree.insert_bf(Some(chosen_index), Some(threshold as usize), Some(0));
    feature_indices.retain(|&value| value != chosen_index);

    let mut feature_impurities:Vec<(f32, usize)> = Vec::new();
    for i in feature_indices.clone() {
        let feature_idx = i;
        let feature_impurity = gini_index(&right_features, &labels, feature_idx, 2);

        feature_impurities.push((feature_impurity, feature_idx));
    }

    println!("chosen feature for right subset");
    let (chosen_gini, chosen_index) = choose_feature(&feature_impurities);
    println!("feature: {:?}\tgini index: {:?}", chosen_index, chosen_gini);

    decision_tree.insert_bf(Some(chosen_index), Some(threshold as usize), Some(1));
    feature_indices.retain(|&value| value != chosen_index);

    // DECISION TREE TRAVERSAL TO CHECK IF TREE WAS BUILT CORRECTLY

    println!("\ntraversing the tree");
    println!("right feature number: {:?}\tright leaf: {:?}", decision_tree.right.as_ref().unwrap().feature.unwrap(), decision_tree.right.as_ref().unwrap().value.unwrap());
    println!("left feature number: {:?} \tleft leaf: {:?}", decision_tree.left.as_ref().unwrap().feature.unwrap(), decision_tree.left.as_ref().unwrap().value.unwrap());
}