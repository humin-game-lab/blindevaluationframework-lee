use tfhe::prelude::*;
use tfhe::{generate_keys, set_server_key, ConfigBuilder, FheBool, FheUint8};

mod circuits;
use crate::circuits::booleanCircuits::*;

static mut SPLIT_FLAGS:Vec<FheUint8> = Vec::new();
static mut SOLUTIONS:Vec<Vec<FheUint8>> = Vec::new();

fn main() -> Result<(), Box<dyn std::error::Error>> {
    use std::time::Instant;
    let mut now = Instant::now();

    // let config = ConfigBuilder::default().build();
    let config = ConfigBuilder::all_disabled().enable_default_integers().build();

    let (keys, server_keys) = generate_keys(config);
    set_server_key(server_keys);

    let mut elapsed = now.elapsed();
    println!("TIMING--config/key gen: {:.2?}\n", elapsed);

    let clear_one:u8 = 1;
    let clear_zero:u8 = 0;
    let clear_ten:u8 = 10;

    let mut fheTrue = FheUint8::try_encrypt(clear_one, &keys)?;
    let mut fheFalse = FheUint8::try_encrypt(clear_zero, &keys)?;
    let mut fheTen = FheUint8::try_encrypt(clear_ten, &keys)?;

    let mut features:Vec<Vec<FheUint8>> = vec![
        vec![fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone()],
        vec![fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone()],
        vec![fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone(), fheTrue.clone()],
        vec![fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone()],
        vec![fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone()],
        vec![fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone()],
        vec![fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone()]

        // vec![fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone()],
        // vec![fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone()],
        // vec![fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheFalse.clone()]
    ];
    let mut labels:Vec<FheUint8> = vec![fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone()];
    // let mut labels:Vec<FheUint8> = vec![fheTrue.clone(), fheTrue.clone(), fheFalse.clone()];
    // let mut labels:Vec<FheUint8> = vec![fheTrue.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone()];

    let mut ohe1 = vec![fheFalse.clone(), fheFalse.clone(), fheFalse.clone(), fheTrue.clone()];
    let mut ohe2 = vec![fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheFalse.clone()];
    let mut ohe3 = vec![fheFalse.clone(), fheFalse.clone(), fheTrue.clone(), fheTrue.clone()];
    let mut ohe4 = vec![fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone()];
    let mut ohe5 = vec![fheFalse.clone(), fheTrue.clone(), fheFalse.clone(), fheTrue.clone()];
    let mut ohe6 = vec![fheFalse.clone(), fheTrue.clone(), fheTrue.clone(), fheTrue.clone()];
    let mut ohe7 = vec![fheTrue.clone(), fheTrue.clone(), fheFalse.clone(), fheFalse.clone()];
    let mut featureOHE:Vec<Vec<FheUint8>> = Vec::new();
    featureOHE.push(ohe1);  // 0001 0
    featureOHE.push(ohe2);  // 0010 1
    featureOHE.push(ohe3);  // 0011 2
    featureOHE.push(ohe4);  // 0100 3
    featureOHE.push(ohe5);  // 0101 4
    featureOHE.push(ohe6);  // 0111 5
    featureOHE.push(ohe7);  // 1100 6

    // ---------- PRIVACY-PRESERVING DECISION TREE TRAINING ----------

    // tupleList: (featureOHE, featureColumn, featureGiniImpurity)
    let mut tupleList:Vec<(Vec<FheUint8>, Vec<FheUint8>, FheUint8)> = Vec::new();

    // // ----- FEATURE SELECTION PHASE ----- 

    let mut now = Instant::now();
    let mut i = 0;
    for feature in features {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();
        for entry in &feature {
            posCount += fheTrue.clone() & entry;
            negCount += fheTrue.clone() ^ entry;
        }
        let maxClassLoss = posCount.min(&negCount);

        // pushing everything to tupleList
        tupleList.push((featureOHE[i].clone(), feature.clone(), maxClassLoss.clone()));
        i += 1;
    }
    let mut elapsed = now.elapsed();
    println!("TIMING--feature impurity calculation: {:.2?}\n", elapsed);

    // finding feature with lowest impurity
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);
        
        let dec_min:u8 = minimum.2.decrypt(&keys);
        let dec_tuple:u8 = tuple.2.decrypt(&keys);
        println!("minimum: {:?} \t tuple: {:?}", dec_min, dec_tuple);

        let dec_ge:u8 = ge.decrypt(&keys);
        let dec_le:u8 = lt.decrypt(&keys);
        println!("root comp: {:?}, {:?}", dec_ge, dec_le);

        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }

        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity: {:.2?}\n", elapsed);

    print!("chosen feature root node: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    // ----- DATA PARTITIONING PHASE -----
    unsafe {
        SPLIT_FLAGS = minimum.1.clone();
    }
    tupleList.remove(0);

    // ----- TREE GROWING PHASE -----
    
    // simulating left node
    let mut i = 0;
    let leftBranchFlag = fheFalse.clone();
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & !(&SPLIT_FLAGS[i]);
                negCount += fheTrue.clone() ^ &entry & !(&SPLIT_FLAGS[i]);
            }
        }
        let maxClassLoss = posCount.min(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- left node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);

        let dec_ge:u8 = ge.decrypt(&keys);
        let dec_le:u8 = lt.decrypt(&keys);
        println!("left comp: {:?}, {:?}", dec_ge, dec_le);

        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--left: {:.2?}\n", elapsed);

    print!("chosen feature left node: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    let mut split_flags_l2_left = minimum.1.clone();
    tupleList.remove(0);

    // simulating right node
    let mut i = 0;
    let rightBranchFlag = fheTrue.clone();
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & (&SPLIT_FLAGS[i]);
                negCount += fheTrue.clone() ^ &entry & (&SPLIT_FLAGS[i]);
            }
        }
        let maxClassLoss = posCount.min(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- right node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--right: {:.2?}\n", elapsed);

    print!("chosen feature right node: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    let mut split_flags_l2_right = minimum.1.clone();
    tupleList.remove(0);

    // // level 3
    // simulating left node
    let mut i = 0;
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & !(&split_flags_l2_left[i]);
                negCount += fheTrue.clone() ^ &entry & !(&split_flags_l2_left[i]);
            }
        }
        let maxClassLoss = posCount.min(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- left node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--left: {:.2?}\n", elapsed);

    print!("chosen feature left node L3: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    // unsafe {
    //     SPLIT_FLAGS = minimum.1.clone();
    // }
    tupleList.remove(0);

    // simulating right node
    let mut i = 0;
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & (&split_flags_l2_left[i]);
                negCount += fheTrue.clone() ^ &entry & (&split_flags_l2_left[i]);
            }
        }
        let maxClassLoss = posCount.min(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- right node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--right: {:.2?}\n", elapsed);

    print!("chosen feature right node L3: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    // unsafe {
    //     SPLIT_FLAGS = minimum.1.clone();
    // }
    tupleList.remove(0);

    // simulating left node
    let mut i = 0;
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & !(&split_flags_l2_right[i]);
                negCount += fheTrue.clone() ^ &entry & !(&split_flags_l2_right[i]);
            }
        }
        let maxClassLoss = posCount.min(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- left node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--left: {:.2?}\n", elapsed);

    print!("chosen feature left node L3: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    // unsafe {
    //     SPLIT_FLAGS = minimum.1.clone();
    // }
    tupleList.remove(0);

    // simulating right node
    let mut i = 0;
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & (&split_flags_l2_right[i]);
                negCount += fheTrue.clone() ^ &entry & (&split_flags_l2_right[i]);
            }
        }
        let maxClassLoss = posCount.min(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- right node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.lt(&tuple.2);
        let lt = minimum.2.ge(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..minimum.0.len() {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
        }
        for i in 0..minimum.1.len() {
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--right: {:.2?}\n", elapsed);

    print!("chosen feature right node L3: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");
    let dec_2:u8 = minimum.2.decrypt(&keys);
    println!("{:?}", dec_2);

    // unsafe {
    //     SPLIT_FLAGS = minimum.1.clone();
    // }
    tupleList.remove(0);

    Ok(())
}
