use tfhe::{FheBool, ConfigBuilder, generate_keys, set_server_key};
use tfhe::prelude::*;

pub fn convert(bits: &[u8]) -> u8 {
    let mut result: u8 = 0;
    bits.iter().for_each(|&bit| {
        result <<= 1;
        result ^= bit;
    });
    result
}

pub fn half_adder(a: &FheBool, b: &FheBool) -> (FheBool, FheBool) {
    let mut sum = a.clone();
    let mut carry = a.clone();

    sum = a ^ b;
    carry = a & b;

    (carry, sum)
}

pub fn full_adder(carry_in: &FheBool, a: &FheBool, b: &FheBool) -> (FheBool, FheBool) {
    let mut carry1 = carry_in.clone();
    let mut carry2 = carry1.clone();
    let mut carry = carry_in.clone();
    let mut sum1 = a.clone();
    let mut sum = a.clone();

    (carry1, sum1) = half_adder(carry_in, a);
    (carry2, sum) = half_adder(&sum1, b);
    carry = carry1 | carry2;

    (carry, sum)
}

pub fn full_subtractor(b_in: &FheBool, a: &FheBool, b: &FheBool) -> (FheBool, FheBool) {
    let mut diff = b_in.clone();
    let mut a1 = a.clone();
    let mut b_out = b_in.clone();

    diff = (a ^ b) ^ b_in;
    a1 = !a;

    // B_Out = A1 & Bin | A1 & B | B & Bin
    b_out = (&a1 & b_in) | (&a1 & b) | (b & b_in);

    (b_out, diff)
}

fn sequential_multiplier_v2(multiplier: &mut Vec<FheBool>, multiplicand: &mut Vec<FheBool>, pad: FheBool) -> Vec<FheBool> {
    let mut accumulator:Vec<FheBool> = Vec::new();

    for i in 0..(multiplicand.len() * 2) {
        accumulator.push(pad.clone());
    }
    for i in 0..multiplicand.len() {
        multiplicand.insert(0, pad.clone());
    }

    for i in 0..multiplicand.len() {
        let mut q0 = multiplier[multiplier.len()-1].clone();

        let mut fhe_carry = pad.clone();
        let mut fhe_sum = pad.clone();

        for i in (0..accumulator.len()).rev() {
            let mut valid_bit = &q0 & &multiplicand[i];
            (fhe_carry, fhe_sum) = full_adder(&fhe_carry, &accumulator[i], &valid_bit);
            std::mem::replace(&mut accumulator[i], fhe_sum);
        }
        left_shift(multiplicand, pad.clone());
        right_shift(multiplier, pad.clone());
    }

    accumulator
}

pub fn right_shift(target: &mut Vec<FheBool>, pad: FheBool) {
    target.pop();
    target.insert(0, pad.clone());
}

pub fn left_shift(target: &mut Vec<FheBool>, pad: FheBool) {
    target.remove(0);
    target.push(pad.clone());
}

pub fn bitWiseComparator(a: &FheBool, b: &FheBool) -> (FheBool, FheBool, FheBool) {
    // return (A>B, A<B, A==B)
    (a & !b, !a & b, !a & !b | a & b)
}

pub fn twoBitComparator(a: &[FheBool], b: &[FheBool]) -> (FheBool, FheBool, FheBool) {
    (
        (&a[0] & !&b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (&a[1] & !&b[1]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]),
        (!&a[0] & &b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & !&a[1] & &b[1] | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]),
        (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1])
    )
}

pub fn threeBitComparator(a: &[FheBool], b: &[FheBool]) -> (FheBool, FheBool, FheBool) {
    (
        (&a[0] & !&b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (&a[1] & !&b[1]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (&a[2] & !&b[2]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]),
        (!&a[0] & &b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & !&a[1] & &b[1] | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & &b[2]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]),
        (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2])
    )
}

pub fn fourBitComparator(a: &[FheBool], b: &[FheBool]) -> (FheBool, FheBool, FheBool) {
    (
        (&a[0] & !&b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (&a[1] & !&b[1]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (&a[2] & !&b[2]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]) & (&a[3] & !&b[3]),
        (!&a[0] & &b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & !&a[1] & &b[1] | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & &b[2]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]) & (!&a[3] & &b[3]),
        (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]) & (!&a[3] & !&b[3] | &a[3] & &b[3])
    )
}

pub fn funcGT(a: &[FheBool], b: &[FheBool]) -> (FheBool, FheBool) {

    // returns (enc(true), enc(false)) or (enc(false), enc(true))
    // returns (enc(1), enc(0)) or (enc(0), enc(1))
    (fourBitComparator(&a, &b).0, fourBitComparator(&a, &b).1 | fourBitComparator(&a, &b).2)
}

pub fn funcLT(a: &[FheBool], b: &[FheBool]) -> (FheBool, FheBool) {

    // returns (enc(true), enc(false)) or (enc(false), enc(true))
    // returns (enc(1), enc(0)) or (enc(0), enc(1))
    (fourBitComparator(&a, &b).1, fourBitComparator(&a, &b).0 | fourBitComparator(&a, &b).2)
}

pub fn funcGT_one(a: &FheBool, b: &FheBool) -> (FheBool, FheBool) {

    // returns (enc(true), enc(false)) or (enc(false), enc(true))
    // returns (enc(1), enc(0)) or (enc(0), enc(1))
    (bitWiseComparator(&a, &b).0, bitWiseComparator(&a, &b).1 | bitWiseComparator(&a, &b).2)
}

pub fn funcLT_one(a: &FheBool, b: &FheBool) -> (FheBool, FheBool) {

    // returns (enc(true), enc(false)) or (enc(false), enc(true))
    // returns (enc(1), enc(0)) or (enc(0), enc(1))
    (bitWiseComparator(&a, &b).1, bitWiseComparator(&a, &b).0 | bitWiseComparator(&a, &b).2)
}

pub fn fourBitComparator_v(a: &Vec<FheBool>, b: &Vec<FheBool>) -> (FheBool, FheBool, FheBool) {
    (
        (&a[0] & !&b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (&a[1] & !&b[1]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (&a[2] & !&b[2]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]) & (&a[3] & !&b[3]),
        (!&a[0] & &b[0]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & !&a[1] & &b[1] | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & &b[2]) | (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]) & (!&a[3] & &b[3]),
        (!&a[0] & !&b[0] | &a[0] & &b[0]) & (!&a[1] & !&b[1] | &a[1] & &b[1]) & (!&a[2] & !&b[2] | &a[2] & &b[2]) & (!&a[3] & !&b[3] | &a[3] & &b[3])
    )
}

// GT and LT functions that take in vectors instead of arrays
pub fn funcGT_v(a: &Vec<FheBool>, b: &Vec<FheBool>) -> (FheBool, FheBool) {

    // returns (enc(true), enc(false)) or (enc(false), enc(true))
    // returns (enc(1), enc(0)) or (enc(0), enc(1))
    (fourBitComparator(&a, &b).0, fourBitComparator(&a, &b).1 | fourBitComparator(&a, &b).2)
}

pub fn funcLT_v(a: &Vec<FheBool>, b: &Vec<FheBool>) -> (FheBool, FheBool) {

    // returns (enc(true), enc(false)) or (enc(false), enc(true))
    // returns (enc(1), enc(0)) or (enc(0), enc(1))
    (fourBitComparator(&a, &b).1, fourBitComparator(&a, &b).0 | fourBitComparator(&a, &b).2)
}

pub fn hotSwap(flag0: &FheBool, flag1: &FheBool, array0: &[FheBool], array1: &[FheBool]) -> (Vec<FheBool>, Vec<FheBool>) {
    let mut newArray0:Vec<FheBool> = Vec::new();
    let mut newArray1:Vec<FheBool> = Vec::new();
    for i in 0..4 {
        newArray0.push(flag0 & &array1[i] | flag1 & &array0[i]);
        newArray1.push(flag0 & &array0[i] | flag1 & &array1[i]);
    }
    (newArray0, newArray1)
}

pub fn findMin(list: &Vec<Vec<FheBool>>) -> Vec<FheBool> {
    let mut currMin = list[0].clone();
    for element in list {
        let (mut gt, mut lt) = funcGT_v(&currMin, element);
        for i in 0..currMin.len() {
            currMin[i] = &gt & &element[i] | &lt & &currMin[i];
        }
    }
    currMin
}

pub fn findMax(list: &Vec<Vec<FheBool>>) -> Vec<FheBool> {
    let mut currMax = list[0].clone();
    for element in list {
        let (mut gt, mut lt) = funcLT_v(&currMax, element);
        for i in 0..currMax.len() {
            currMax[i] = &gt & &element[i] | &lt & &currMax[i];
        }
    }
    currMax
}

// pub fn findMin(list:&mut Vec<(Vec<FheBool>, Vec<FheBool>, Vec<FheBool>)>) -> (Vec<FheBool>, Vec<FheBool>, Vec<FheBool>) {
//     let mut currMin = list[0].clone();
//     for mut tuple in list {
//         let (mut gt, mut lt) = funcGT_v(&currMin.2, &tuple.2);
//         for i in 0..4 {
//             currMin.0[i] = &gt & &tuple.0[i] | &lt & &currMin.0[i];
//             currMin.1[i] = &gt & &tuple.1[i] | &lt & &currMin.1[i];
//             currMin.2[i] = &gt & &tuple.2[i] | &lt & &currMin.2[i];

//             tuple.0[i] = &gt & &tuple.0[i] | &lt & &currMin.0[i];
//             tuple.1[i] = &gt & &tuple.1[i] | &lt & &currMin.1[i];
//             tuple.2[i] = &gt & &tuple.2[i] | &lt & &currMin.2[i];
//         }
//     }
//     currMin
// }