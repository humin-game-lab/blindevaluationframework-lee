static mut SPLIT_FLAGS:Vec<u8> = Vec::new();

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let clear_one:u8 = 1;
    let clear_zero:u8 = 0;
    let clear_four:u8 = 4;

    let mut features:Vec<Vec<u8>> = vec![
        vec![clear_one.clone(), clear_zero.clone(), clear_one.clone(), clear_zero.clone(), clear_zero.clone()],
        vec![clear_one.clone(), clear_zero.clone(), clear_one.clone(), clear_one.clone(), clear_zero.clone()],
        vec![clear_zero.clone(), clear_zero.clone(), clear_zero.clone(), clear_zero.clone(), clear_zero.clone()]
    ];
    let mut labels:Vec<u8> = vec![clear_one.clone(), clear_zero.clone(), clear_one.clone(), clear_zero.clone(), clear_zero.clone()];

    let mut ohe1 = vec![clear_zero.clone(), clear_zero.clone(), clear_zero.clone(), clear_one.clone()];
    let mut ohe2 = vec![clear_zero.clone(), clear_zero.clone(), clear_one.clone(), clear_zero.clone()];
    let mut ohe3 = vec![clear_zero.clone(), clear_one.clone(), clear_zero.clone(), clear_zero.clone()];
    let mut featureOHE:Vec<Vec<u8>> = Vec::new();
    featureOHE.push(ohe1);
    featureOHE.push(ohe2);
    featureOHE.push(ohe3);

    // ---------- PRIVACY-PRESERVING DECISION TREE TRAINING ----------

    // tupleList: (featureOHE, featureColumn, featureGiniImpurity)
    let mut tupleList:Vec<(Vec<u8>, Vec<u8>, u8)> = Vec::new();

    // ----- FEATURE SELECTION PHASE ----- 

    let mut now = Instant::now();
    let mut i = 0;
    for feature in features {
        // feature impurity calculation
        let mut posCount = clear_zero.clone();
        let mut negCount = clear_zero.clone();
        for entry in &feature {
            posCount += clear_one.clone() & entry;
            negCount += clear_one.clone() ^ entry;
        }
        let maxClassLoss = clear_four.clone() - posCount.max(&negCount);

        // pushing everything to tupleList
        tupleList.push((featureOHE[i].clone(), feature.clone(), maxClassLoss.clone()));
        i += 1;
    }
    let mut elapsed = now.elapsed();
    println!("TIMING--feature impurity calculation: {:.2?}\n", elapsed);

    // finding feature with lowest impurity
    let mut now = Instant::now();

    let mut minimum:(Vec<u8>, Vec<u8>, u8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.ge(&tuple.2);
        let lt = minimum.2.lt(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..4 {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];

            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity: {:.2?}\n", elapsed);

    // ----- DATA PARTITIONING PHASE -----
    unsafe {
        SPLIT_FLAGS = minimum.1.clone();
    }
    tupleList.remove(0);

    // ----- TREE GROWING PHASE -----
    
    // simulating left node
    let mut i = 0;
    let leftBranchFlag = clear_zero.clone();
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = clear_zero.clone();
        let mut negCount = clear_zero.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += clear_one.clone() & &entry & !(&leftBranchFlag ^ &SPLIT_FLAGS[i]);
                negCount += clear_one.clone() ^ &entry & !(&leftBranchFlag ^ &SPLIT_FLAGS[i]);
            }
        }
        let maxClassLoss = clear_four.clone() - posCount.max(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- left node
    let mut now = Instant::now();

    let mut minimum:(Vec<u8>, Vec<u8>, u8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.ge(&tuple.2);
        let lt = minimum.2.lt(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..4 {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];

            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--left: {:.2?}\n", elapsed);

    print!("chosen feature left node: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");

    // simulating right node
    let mut i = 0;
    let rightBranchFlag = fheTrue.clone();
    for tuple in &mut tupleList {
        // feature impurity calculation
        let mut posCount = fheFalse.clone();
        let mut negCount = fheFalse.clone();

        for entry in tuple.1.clone() {
            unsafe {
                posCount += fheTrue.clone() & &entry & (&rightBranchFlag & &SPLIT_FLAGS[i]);
                negCount += fheTrue.clone() ^ &entry & (&rightBranchFlag & &SPLIT_FLAGS[i]);
            }
        }
        let maxClassLoss = fheFour.clone() - posCount.max(&negCount);

        tuple.2 = maxClassLoss;
    }

    // finding feature with lowest impurity -- right node
    let mut now = Instant::now();

    let mut minimum:(Vec<FheUint8>, Vec<FheUint8>, FheUint8) = tupleList[0].clone();
    for tuple in &mut tupleList {
        let ge = minimum.2.ge(&tuple.2);
        let lt = minimum.2.lt(&tuple.2);
        // hotswap technique for min/maxing a list of tuples
        let prev_min = minimum.clone();
        for i in 0..4 {
            minimum.0[i] = &ge & &tuple.0[i] | &lt & &minimum.0[i];
            minimum.1[i] = &ge & &tuple.1[i] | &lt & &minimum.1[i];

            tuple.0[i] = &ge & &prev_min.0[i] | &lt & &tuple.0[i];
            tuple.1[i] = &ge & &prev_min.1[i] | &lt & &tuple.1[i];
        }
        minimum.2 = &ge * &tuple.2 + &lt * &minimum.2;
        tuple.2 = &ge * &prev_min.2 + &lt * &tuple.2;
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--finding feature with lowest impurity--right: {:.2?}\n", elapsed);

    print!("chosen feature right node: ");
    for bit in &minimum.0.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   print!(" ");
    for bit in &minimum.1.clone() {
        let dec_bit:u8 = bit.decrypt(&keys);
        print!("{:?}", dec_bit);
    }   println!("");

    // ---------- PREDICTION ----------

    let mut validation:Vec<Vec<FheUint8>> = vec![
        vec![fheTrue.clone(), fheTrue.clone(), fheTrue.clone()],            // label = 1
        vec![fheFalse.clone(), fheTrue.clone(), fheTrue.clone()],           // label = 1
        vec![fheFalse.clone(), fheFalse.clone(), fheFalse.clone()],         // label = 0
        vec![fheFalse.clone(), fheTrue.clone(), fheFalse.clone()],          // label = 1
        vec![fheFalse.clone(), fheFalse.clone(), fheTrue.clone()],          // label = 0
        vec![fheFalse.clone(), fheTrue.clone(), fheTrue.clone()],           // label = 0
        vec![fheTrue.clone(), fheTrue.clone(), fheTrue.clone()],            // label = 1
        vec![fheTrue.clone(), fheFalse.clone(), fheTrue.clone()],           // label = 1
        vec![fheTrue.clone(), fheTrue.clone(), fheFalse.clone()],           // label = 1
        vec![fheTrue.clone(), fheTrue.clone(), fheFalse.clone()]            // label = 0
    ];

    let mut now = Instant::now();
    let mut labels:Vec<FheUint8> = Vec::new();
    for test in &validation {
        let mut label = &test[0] & (&test[1] & fheTrue.clone() | !&test[1] & fheFalse.clone()) | !&test[0] & (&test[2] & fheTrue.clone() | !&test[2] & fheFalse.clone());



        labels.push(label);
    }

    let mut elapsed = now.elapsed();
    println!("TIMING--prediction: {:.2?}\n", elapsed);

    for label in labels {
        let dec_bit:u8 = label.decrypt(&keys);
        print!("{:?} ", dec_bit);
    }   println!("");

    Ok(())
}